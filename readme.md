# SAM vLab with for Netops People
This is a Gitlab Repository to do an easy Virtual Lab Environment for a Network Provisioning System ( https://www.ansible.com/ ) based on Open Source Software.

To Follow the Instrutions of this Virtual Lab:
Open your Browser

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-netdevops-ansible/master?grs=gitlab&t=black

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-netdevops-ansible
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Virtual Lab Environment Setup:

Requirements:
  * 8 GB Memory (minimal)
  * 2 x CPU Cores
  * Virtualbox

vLAB Setup:
  * devnetops (Ubuntu 16.04 )
    * Complete Infrastructure Example Toolchain for a Network Automation System
  * ansible (Ubuntu 16.04 with Ansible installed)
    * Developer Workstation
  * device-nxos-1 (Cisco Virtual NXOS for Testing Environment)

Option 1 Vagrant (https://www.vagrantup.com/) (local):
  * Downlad and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb?_ga=2.85169500.497796412.1511295690-1446073050.1511295690
      * dpkg -i vagrant_2.0.1_x86_64.deb

  * Import the Cisco Virtual NXOS Environment
    * Download the Vagrant Box
    * Import the Downloaded Box
      * vagrant box add --name nxos/7.0.3.I7 nxosv-final.7.0.3.I6.1.box
  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-netdevops-ansible
  * Setup the vLAB Environment with Vagrant
    * cd sam-vlab-devops-ansible
    * cd vlab
    * cd vagrant
    * vagrant up
  * Check the vLAB Setup
    * vagrant status
  * Login to work with a Node
    * vagrant ssh ansible
