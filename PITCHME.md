<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


# Netdevops vLAB
### Netdevops
---
# Netdevops Toolchain
* Netdevops
* Version 20180424

---

# Netdevops Toolchain vLAB
* Voraussetzungen vLAB
* Installation vLAB
* Devnetops Einführung
* Ansible Adhoc Mode
* Editoren
  * ATOM
  * Vistualstudio Code
  * Extensions
* Markup Languages
  * YAML
  * JSON
  * XML
* Versionierung
  * Git
  * Gitlab
  * TFS
* Ansible / Puppet
  * Inventory
  * Playbooks
  * Jinja2
  * Rollen
  * SSH / Netconf / YANG
* CI / CD Pipelines
  * Gitlab
  * Jenkins
  * Circle CI
* Visualisierung
  * Gitlab / TFS
  * Graylog / Splunk
  * Foreman / Ansible / AWX / Stackstorm
* Collaboration
 * Email
 * Spark
---
# Devnetops Voraussetzungen vLAB
* VM ( Virtualbox)
* 2 x CPU Core
* 8 GB RAM
---
## Devnetop vLAB Überblick (Simple)
![Netdevops Überblick](_images/sam-vlab-overview.png)

---
## Überblick vLAB Ansible (Full)

![Architektur Full](_images/sam-vlab-devnetops-toolchain.png)

---
## Installation devnetops.local (Manual)
* Ubuntu 16.04 LTS Basis
* Install Base Packages
    * sudo apt-get update
    * sudo apt-get install -y git
    * sudo apt-get install -y curl
    * sudo apt-get install -y links
    * sudo apt-get install -y tree
    * sudo apt-get install -y avahi-daemon libnss-mdns
    * sudo apt-get install -y software-properties-common
 * Install Gitlab:
 * Install Gitlab Runner
 * Install Docker

---  
## Installation ansible.local (Manual)
* Ubuntu 16.04 LTS Basis
* Install Ansible:
    * sudo apt-get update
    * sudo apt-get install -y git
    * sudo apt-get install -y curl
    * sudo apt-get install -y links
    * sudo apt-get install -y tree
    * sudo apt-get install -y software-properties-common
    * sudo apt-add-repository ppa:ansible/ansible
    * sudo apt-get update
    * sudo apt-get install -y ansible
---
## Installation Network Device Cisco 9000v (Vagrant)
* Download Nexus 9000v from Cisco Website (Contract needed)
* vagrant box add --name nxos/7.0.3.I7 nxosv-final.7.0.3.I7.3.box
* vagrant init
* vagrant up
* vagrant up
---
# Installation Device Cumulus (Vagrant)
* Change Directory to vlab/vagrant-cumulus-vx
* vagrant init CumulusCommunity/cumulus-vx
* vagrant up

---
## Devnetops vLAB Complete Setup (Vagrant)(Empfohlen)
* Download Nexus 9000v from Cisco Website (Contract needed)
* vagrant box add --name nxos/7.0.3.I7 nxosv-final.7.0.3.I7.3.box
* git clone https://www.gitlab.com/rstumper/sam-vlab-devnetops-ansible
* cd vlab/vagrant
* vagrant up
* vagrant ssh devnetops
* vagrant ssh ansible
* vagrant ssh devicenxos1
* Username: ansible
* Password: ansible
---
## Devnetops vLAB Complete Setup (Docker)
* git clone https://www.gitlab.com/rstumper/sam-vlab-devnetops-ansible
* cd vlab/docker
* docker-compose up
* docker run -it sam-vlab-devops-ansible
* Username: vagrant
* Password: vagrant
---
## Ansible (Standalone) vLAB Docker from Dockerhub

sudo docker run -it -t -v /home/sam/ansible:/root/ansible corbanr/ansible

---
## Einführung Ansible
#### Ein Open Source Community Projekt gesponsert von Red Hat . Sehr einfach gehalten und kann vom Server Systemen bis zu Netzwerkkomponenten für die Automatisierung von IT verwendet werden. (http://www.ansible.com)

* Ist ein Framework zum Konfigurationsmanagement
* Based on Python
* Client / Server Architektur
* kein Agent Notwendig
* Prinzip der Desired State Configuration
* Zur Kommunikation werden Standard Protokolle verwendet
  * SSH
  * SNMP
  * WinRM
  * Netconf
* kann zur Ausführung von Ad Hoc Commandos benutzt werden
* YAML und JSON für die Beschreibung der Konfiguration => Playbooks
* Template Engine: Jinja2

---
## Desired State Configuration
* Auch durch mehrmalige Ausführung sollte kein anderes Ergebnis Rauskommen
* schwer bei Netzwerk Devices


---
## First Steps in Ansible
* Check Ansible Version:
	* ansible --version

```
ansible 2.3.0.0
  config file = /etc/ansible/ansible.cfg
  configured module search path = Default w/o overrides
  python version = 2.7.12 (default, Nov 19 2016, 06:48:10) [GCC 5.4.0 20160609]
```
---
## Ansible Inventory File Example

```
[localhost]
localhost

[cisco-nxos]
cisco-nxos-1.local ansible_host=cisco-nxos-1.local
```

---
## Test Ansible connection to Hosts
* Check the Local Host using Module Ping
```
ansible localhost -m ping
```
* Check the Client Connection
```
ansible --inventory-file=ansible-inventory cisco-nxos-1.local -m ping
```

---
## Ansible Adhoc Mode
* Using Ansible to Execute a Command

```
ansible localhost -a "/bin/echo hello"
```
* Using Ansible to Execute a Command on Remote Host with Inventory File

```
ansible --inventory-file=ansible-inventory client.local -a 'netstat' -u ansible -k
```

---
## Install Package with the Apt Module

```
ansible --inventory-file=ansible-inventory client.local -m apt -a "name=ntp state=present" -u ansible -k
```

* Need to Become Superuser to Install a Package
```
ansible --inventory-file=ansible-inventory client.local -m apt -a "name=ntp state=present" -u ansible -k --become
```

* Set Superuser Privileges
```
usermod -G sudo ansible
cp /etc/sudoers.d/90-cloud-init /etc/sudoers.d/91-ansible
```

---
# Ansible Inventory
##### Um Informationen über die zu verwaltende Infrastruktur zu bekommen gibt es in Ansible das Inventory System

* Inventory Hostfile (ini / YAML)
* Inventory Hirachie  (YAML oder JSON)
* Dynamische Inventory (JSON)
  * External Script

---
# Ansible Inventory
```
├── inventory
│   ├── first-inventory
│   ├── group_vars
│   │   └── all.yml
│   ├── host_vars
│   │   └── devicenxos1.yml
│   └── network-inventory
```
---
# Ansible Inventory
``` YAML
all:
  children:
    standorte:
      children:
        netzwerk-standort-a:
          children:
            net-standort-a-core:
              hosts:
                devicenxos1:
```

---
## Ansible Überblick Arbeitsweise
![Ansible Überblick Arbeitsweise](_images/ansible-ueberblick.png)

---
## Ansible Überblick Arbeitsweise
![Ansible Überblick Arbeitsweise](_images/ansible-ueberblick-termini.png)

---
## First Ansible Playbook (Linux)

```YAML
---
# My First playbook.yml
- name: Install an Apache Package
  hosts: localhost
  connection: local
  sudo: yes
  tasks:
    - name: Install apache2
      apt: name=apache2 state=present
      tags: apache2
    - name: Start Apache
      service: name=apache2 state=started
      tags: apache-service

```

---

# First Ansible Playbook (Cisco)

```YAML
---
#  This is my first Playbook with Cisco Command Module

- hosts: cisco-nxos
  gather_facts: no
  connection: local

  tasks:
  - name: SYS | Define provider
    set_fact:
      provider:
#        host: "{{ inventory_hostname }}"
        host: "{{ ansible_host }}"
        username: "{{ ansible_ssh_user }}"
        password: "{{ ansible_ssh_password }}"

  - name: IOS | Show version
    ios_command:
      host: "{{ ansible_ssh_host }}"
      username: "{{ ansible_ssh_user }}"
      password: "{{ ansible_ssh_password }}"
      commands:
        - show version
    register: cmdreplay

  - debug: msg="{{ cmdreplay.stdout }}"

```

---
## YAML
##### (Yet Another Markup Language)

* Eine Strukturierte Sprache zur Bschreibung von Daten
* Ähnlich XML oder JSON
* aber leichter zu Lesen
* Sowas wie Markdown nur für Daten
* Grundprinzip Key / Value Paare
* http://www.yaml.org/start.html

---
## YAML
##### (Example)

```yaml
# Ein Beispiel

name: "Roland Stumpner"
job: "IT Techniker"
employed: True
languages:
  German: Native
  English: Fluent
  Spanish: Novice
  python: Novice
  ruby: Novice
education: Maitrise
favorite foods:
  - Schweinsbraten
  - Steak
```
---
## YAML
##### (Example Network)

```yaml
# Ein Beispiel für eine Netzwerk Konfigurationsbeschreibung
interface:
   "gi0/01":
     description: "Cust: mypc"
     mtu: 1500
     ip_address: 10.10.1.1
     switchport: "no switchport"
vlan:
```
---

## Editor Microsoft Visual Studio Code
* Leichtgewichtige Entwicklungsumgebung die Erweitert werden kann und viele Sprachen unterstützt.  
* Verfügbar für:
  * Microsoft Windows
  * Linux
  * MacOS
* Unterstützung:
  * YAML
  * XML
  * Python

 Link:
 https://code.visualstudio.com/

---
## VS Code
![VSCode Überblick](_images/vscode-ov.png)

---
## VS Code Extensions
Extenstions:
* Ansible Extension:
https://marketplace.visualstudio.com/items?itemName=vscoss.vscode-ansible

* Gitlens
https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

* Gitlab Pipeline Monitor https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor

---
## Editor ATOM
* Javascript basierte Entwicklungsumgebung die Erweitert werden kann und viele Sprachen unterstützt.  
* Verfügbar für:
  * Microsoft Windows
  * Linux
  * MacOS
* Unterstützung:
  * YAML
  * XML
  * Python

Link:
https://atom.io/

---
## ATOM
![ATOM Überblick](_images/atom-ov.png)

---
## ATOM Extensions
Extenstions:
* Ansible Extension:
https://marketplace.visualstudio.com/items?itemName=vscoss.vscode-ansible

* Gitlens
https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

* Gitlab Pipeline Monitor https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor

---
## GIT

###### Ist eine verteilte Versionsverwaltungssoftware basierend auf Open Source (https://git-scm.com/). Bei einer Verteilten Versionsverwaltung hat jeder Mitarbeiter ein lokales Repository mit einer lokalen Versionsgeschichte gespeichert . Diese kann mit einem anderen Teilnehmer oder einem Zentralen Repository abgleichen werden.

* Entwickelt von Linus Torwalds
* seit 2005
* Open Source
* Verteilte Versionsverwaltung
* jeder Besitzt eine Lokale Kopie des Repositorys
---
## GIT
![GIT Brunches](_images/git-workflow.png)

---
## GIT Cheat Sheet
* git init (Erstellen eines Repositorys)
* git clone https://repository (Clonen eines Repositorys)
* git brunch  (Erzeugen eines Brunches)
* git add --all (Hinzufügen von Änderungen)
* git commit -m "changed Code what ?" (Commit Erzeugen)
* git push origin brunch (Brunch an das Repository Übertragen)
* git merge (Merge Request Erzeugen)
* git Pull (Änderungen vom Repository holen)
* git show (Zeige Änderungen des letzten Commits)
* git log --oneline

---
## GIT CLI

Copy from Readme + Example

---
## GIT VSCode
 Workflow

---
## Gitlab
* Repository
* Commits
* Issues

---
## CI / CD Pipelines
##### Eine Methode aus dem Entwicklerumfeld um Code Automatisiert zu Bauen  / Testen und Automatisiert auf Development / Test und Produktion zu deployen.

![CI/CD Pipeline](_images/ci-cd-pipeline.png)

---
## CI / CD Pipelines

---
## CI / CD Pipelines

```YAML
# .gitlab-ci.yml
# List of Stages
stages:
  - review
  - build-config
  - check
  - testing
  - staging
  - production

# Setzen von Umgebungsvariablen
variables:
  ANSIBLE_FORCE_COLOR: 'true'
  GIT_SSL_NO_VERIFY: "1"
  ANSIBLE_HOST_KEY_CHECKING: 'false'

# Stage Review
playbook-review:
  stage: review
  # Starte Docker Container mit Clone Repository
  image: fhooe/ansible:latest
  script:
    - 'ansible-playbook -i inventory/fhooe-network-inventory fhooe.yml --syntax-check'

# Stage Build Config
playbook-build-config:
  stage: build-config
  image: fhooe/ansible:latest
# Speichere die Dateien in diesem Verzeichnis in den Artifacts
  artifacts:
    when: always
    paths:
      - roles/backup/backup/
      - roles/backup/config/
      - roles/backup/state/
      - roles/build-config-cisco/config/
      - roles/generate-ios-config/configs-generated/
      - roles/build-config-librenms/build-mysql/
      - roles/build-config-checkpoint/config/
  script:
    - 'ansible-playbook -i inventory/fhooe-network-inventory fhooe.yml --tags generate-config-ios,build-config-cisco,build-config-librenms,build-config-checkpoint'

# Stage Check
playbook-check:
  stage: check
  image: fhooe/ansible:latest
  only:
    - check
    - testing
    - staging
    - production
  artifacts:
    when: always
    paths:
      - roles/backup/backup/
      - roles/generate-ios-config/configs-generated/
  script:
    - 'ansible-playbook -i inventory/fhooe-network-inventory fhooe.yml --limit zen-network,wel-w-network-ca --check --vault-password-file /opt/vault-password'

# Stage Testumgebung
playbook-testing:
  stage: testing
  image: fhooe/ansible:latest
  only:
    - testing
  script:
    - 'ansible-playbook -i inventory/fhooe-network-inventory fhooe.yml --limit zen-network-testing --vault-password-file /opt/vault-password'

# Stage Staging
playbook-staging:
  stage: staging
  image: fhooe/ansible:latest
  only:
    - staging
    - production
  script:
    - 'ansible-playbook -i inventory/fhooe-network-inventory fhooe.yml --limit zen-network-staging --vault-password-file /opt/vault-password'

# Stage Produktion
playbook-production:
  stage: production
  image: fhooe/ansible:latest
  only:
    - production
  when: manual
  artifacts:
    when: always
    paths:
      - roles/backup/backup/
      - roles/generate-ios-config/configs-generated/
  script:
    - 'ansible-playbook -i inventory/fhooe-network-inventory fhooe.yml --limit zen-network,wel-w-network-ca --vault-password-file /opt/vault-password'
```

---
## CI / CD Execution Environment
![](_images/cicd-execution-environment.png)

---
## XML
```XML
<config xmlns:xc="urn:ietf:params:xml:ns:netconf:base:1.0">
<system xmlns="urn:ietf:params:xml:ns:yang:ietf-system">
<ntp>
<enabled>true</enabled>
<server>
<name>ntp1</name>
<udp><address>127.0.0.1</address></udp>
</server>
</ntp>
</system>
</config>
```
---
## JSON
##### (Javascript Object Notation)

* ein Leichtgewichtiges Datenaustauschformat
* leicht von Menschen lesbar
* noch leichter für Maschinen

---
## JSON
##### (Beispiel)

```json
# Ein Beispiel

name :== "Roland Stumpner"
job :== "IT Techniker"
employed :== true
languages :=={
  "German":"Native",
  "English":"Fluent",
  "Spanish":"Novice",
  "python":"Novice",
  "ruby":"Novice",
}
education := Maitrise
favorite foods[
  "Schweinsbraten",
  "Steak",
]
```
* Strings müssen unter Hochkomma stehen
---
## NETCONF
##### Ein Protokoll das von der IETF Spezifiziert wurde um Netzwerkkomponenten zu Installieren / Manipulieren und zu Konfigurieren.

* Spezifiziert seit 2006
* NETCONF Base Protocol (SOAP)
* NETCONF over SSH
* Unterteilung in Configuration / Operational Data
* Extensions
  * Notifications
  * Partial Locking
  * YANG

---
## Configuration Data
```
show run config
```
## Operational Data
```
show interface gi1/1/23
```

---
## YANG

---
## Protokolle

| Standard | Netconf |Restconf|gRPC|
|           | SSH oder SOAP| HTTPS | HTTPS |


---
---
## Devnetops Einführung

---
## Links
Ansible Docs (http://docs.ansible.com/)
Ciscolive365 (https://www.ciscolive.com/global/on-demand-library/?#/ )
* BRKSDN-2666
* DEVNET-1365
* BRKDEV-2000

Cisco Hpreston on Github:
https://github.com/hpreston/netdevops_demos



---
<!---
```python
---
# This playbook contains common plays that will be run on all nodes.

- name: Install ntp
  yum: name=ntp state=present
  tags: ntp

- name: Configure ntp file
  template: src=ntp.conf.j2 dest=/etc/ntp.conf
  tags: ntp
  notify: restart ntp

- name: Start the ntp service
  service: name=ntpd state=started enabled=yes
  tags: ntp

- name: test to see if selinux is running
  command: getenforce
  register: sestatus
changed_when: false
```
---

```ini
# Ansible Inventory File Realworld Example

[localhost]
localhost

[all:vars]
ansible_ssh_user=netadmin
ansible_ssh_pass=Adminpa$$word


[standorte:children]
netzwerk-standort-a

[netzwerk-standort-a:children]
net-standort-a-core
#net-standort-a-pe
#zen-network-ca-access
#zen-network-staging

[net-standort-a-core]
standort-a-core ansible_host=10.0.2.15

```
---
---
# Enable SSH Key Auth

* Install SSHPASS Tool
    * sudo apt-get install sshpass

---

```python
ubuntu@ubuntu-xenial:~/.ssh$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCXDhJmpolArj6GW8ljgalic1JmNdNgJanWeD+whBnQ+SGT+I1N+EWjA49rnVOI0ZuV6ewaiRCvNixN9m2ur2XcaOuv45asYzT26dQpojB/wrcNMtcFMAV5ZsH4Ex27LlQuv4HNyS/f3z7aWxZQKLY5E5n3mtmfvSForhIvs637Li0rEdHjpZ5TiBHXwNoJx4OydkiZ9F4jekemHONC1qrlFaYQnS/PWZcCgTA/7Z1YVgf2YBew74/VUkrrvEKxdsku68mK6jhQDGWKY2kUkN6urIfwo1E5dWFY+Nfe7o//saFEbvRFzNPBbu/Kvum4A8QKKEgHACweIhVwi0Psa65z vagrant
```
-->

username netadmin password 0 Adminpa$$word role network-admin
